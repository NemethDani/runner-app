package hu.braininghub.bh06.runner.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import hu.braininghub.bh06.runner.model.Gender;
import hu.braininghub.bh06.runner.model.Runner;
import hu.braininghub.bh06.runner.model.Vehicle;


public class DefaultRunnerDao extends DefaultBaseDao<Runner>  implements RunnerDao {

	public DefaultRunnerDao() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}



	@Override
	public void create(Runner runner) {
		Boolean isAutoCommitEnabled = false;
		try {
			isAutoCommitEnabled = getConnection().getAutoCommit();
			getConnection().setAutoCommit(false);
				try(PreparedStatement pstm = getConnection().prepareStatement("INSERT into RUNNER (isActive, firstName, lastName, gender, birthdate, vehicle)"+"values(?,?,?,?,?,?)");){
					
					pstm.setBoolean(1, runner.isActive() );
					pstm.setString(2, runner.getFirstName());
					pstm.setString(3, runner.getLastName());
					pstm.setString(4, runner.getGender().toString());
					pstm.setDate(5, runner.getBirthdate());
					pstm.setString(6, runner.getVehicle().toString());
					pstm.executeUpdate();
					getConnection().commit();
				}
			} catch (SQLException e) {
				try {
					getConnection().rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
					throw new RuntimeException("Error during rollback..." + e1.getMessage());
				}
				e.printStackTrace();
				throw new RuntimeException("Error during save..." + e.getMessage());
			}
		}
				


	@Override
	public Runner read(Long id) {
		PreparedStatement pstm=null;
		ResultSet rs=null;
		
	try {pstm = getConnection().prepareStatement("Select * from RUNNER where id=?");
	pstm.setLong(1, id);
	rs = pstm.executeQuery();
	
	boolean hasRow = rs.next();
	
	if(hasRow) {
		Runner runner = new Runner(rs.getBoolean("isActive"), rs.getString("firstName"), rs.getString("lastName"), Gender.valueOf(rs.getString("gender")),rs.getDate("birthdate"), Vehicle.valueOf(rs.getString("vehicle")));
		return runner;
	}else {
		throw new RuntimeException("There is no runner with this id"); 
	}
	
}catch (SQLException e) {
	e.printStackTrace();
	throw new RuntimeException("Error during retrieve"+e.getMessage());
	}
finally {
	
	if(rs!=null) {
		try {
			rs.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	if (pstm != null) {
		try {
			pstm.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
}


}

	@Override
	public List<Runner> readAll() {
		// TODO Auto-generated method stub
		return super.readAll();
	}

	@Override
	public void update(Runner object) {
		// TODO Auto-generated method stub
		super.update(object);
	}

	@Override
	public void delete(Long id) {
		boolean isAutoCommitEnabled = false;
		try {
			isAutoCommitEnabled = getConnection().getAutoCommit();
			getConnection().setAutoCommit(false);
				try(PreparedStatement pstmt = getConnection().prepareStatement("DELETE FROM RUNNER WHERE ID = ?")) {
			pstmt.setLong(1, id);
			pstmt.executeUpdate();
			getConnection().commit();
		}
	} catch (SQLException e) {
		try {
			getConnection().rollback();
		} catch (SQLException e1) {
			e1.printStackTrace();
			throw new RuntimeException("Error during rollback..." + e1.getMessage());
		}
		e.printStackTrace();
		throw new RuntimeException("Error during delete..." + e.getMessage());
	}
	
}
	

}
