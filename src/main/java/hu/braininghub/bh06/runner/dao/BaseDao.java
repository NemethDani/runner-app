package hu.braininghub.bh06.runner.dao;

import java.util.List;

import hu.braininghub.bh06.runner.model.BusinessObject;

public interface BaseDao<T extends BusinessObject> {
	
	void create(T object);
	T read(Long id);
	List<T> readAll();
	void update(T object);
	void delete(Long id);

}
