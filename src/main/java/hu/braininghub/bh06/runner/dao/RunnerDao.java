package hu.braininghub.bh06.runner.dao;

import hu.braininghub.bh06.runner.model.BusinessObject;
import hu.braininghub.bh06.runner.model.Runner;

public interface RunnerDao extends BaseDao<Runner> {

}
