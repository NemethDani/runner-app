package hu.braininghub.bh06.runner.model;

import java.sql.Date;

public class Runner extends BusinessObject{
	
	private String firstName;
	private String lastName;
	private Gender gender;
	private Date birthdate;
	private Vehicle vehicle;
	
	public Runner(boolean isActive, String firstName, String lastName, Gender gender, Date birthdate,
			Vehicle vehicle) {
		super(isActive);
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.birthdate = birthdate;
		this.vehicle = vehicle;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	
	



}
