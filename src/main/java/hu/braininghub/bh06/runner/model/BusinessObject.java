package hu.braininghub.bh06.runner.model;


public class BusinessObject {
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	private long id;
	private boolean isActive;
	public BusinessObject(boolean isActive) {
		super();
		
		this.isActive = true;
	}
	
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	

}
