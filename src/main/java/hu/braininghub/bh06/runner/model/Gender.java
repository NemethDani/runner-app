package hu.braininghub.bh06.runner.model;

public enum Gender {
	
	MALE, FEMALE, OTHER

}
