package App;

import java.sql.Date;

import hu.braininghub.bh06.runner.dao.BaseDao;
import hu.braininghub.bh06.runner.dao.DefaultRunnerDao;
import hu.braininghub.bh06.runner.model.Gender;
import hu.braininghub.bh06.runner.model.Runner;
import hu.braininghub.bh06.runner.model.Vehicle;

public class App {
	
	public static void main(String[] args) throws Exception {
		
		DefaultRunnerDao dao=new DefaultRunnerDao();
		
		Runner runner=new Runner(true, "Anka", "Manka", Gender.MALE, Date.valueOf("2015-03-31"), Vehicle.BIKE);
		
		//dao.create(runner);
		dao.delete(2L);
	}

}
